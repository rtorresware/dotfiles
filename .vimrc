set nocompatible

filetype off

call plug#begin()

Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'scrooloose/nerdcommenter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

"Powerline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"Python
Plug 'davidhalter/jedi-vim'
Plug 'python-mode/python-mode'
Plug 'vim-scripts/indentpython.vim'

"Colors
Plug 'vim-scripts/mayansmoke'
Plug 'kien/rainbow_parentheses.vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'ryanoasis/vim-devicons'
Plug 'altercation/vim-colors-solarized'

"Tmux
Plug 'christoomey/vim-tmux-navigator'

call plug#end()

"Rainbow
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

let g:NERDCustomDelimiters = {
	\ 'kivy': { 'left': '#' }
\ }

let NERDTreeIgnore = ['\.pyc$', '\.pyo$', '\~$']
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|\.pyc\|\~\|libs\|cache'


"Airline
let g:airline_theme='papercolor'
set laststatus=2
let g:airline_powerline_fonts = 1
"let g:airline_symbols_ascii = 1

"Python mode
let g:pymode_rope = 0
let g:pymode_lint = 0
let g:pymode_breakpoint = 0
let g:pymode_virtualenv = 0
let g:pymode_run = 0
let g:pymode_doc = 0
let g:pymode_folding = 0

filetype plugin indent on
syntax on

set relativenumber
set number
set autoindent

imap jk <Esc>

set path+=**

set wildmenu
set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab

set background=light
colorscheme PaperColor 
"colorscheme solarized 
let g:rainbow_active = 1
set t_Co=256
let g:nerdtree_tabs_open_on_gui_startup=0

set foldmethod=syntax
set hlsearch

let mapleader = "\<Space>"
nnoremap <Leader>w :w<CR>
let &t_ut=''


"Custom functions
com! FormatJSON %!python -m json.tool

set path+=$libs

highlight Normal ctermbg=none
highlight NonText ctermbg=none

